resource "aws_eks_node_group" "group" {
  for_each = {
    for node_group in var.node_groups : node_group["name"] => node_group
  }

  node_group_name = each.value["name"]
  cluster_name    = aws_eks_cluster.cluster.name
  instance_types  = each.value["instance_types"]
  node_role_arn   = aws_iam_role.eks_worker_node[each.key]["arn"]
  ami_type        = each.value["ami_type"]
  capacity_type   = each.value["capacity_type"]
  subnet_ids      = each.value["subnet_ids"]

  launch_template {
    name    = aws_launch_template.node_group[each.key]["name"]
    version = aws_launch_template.node_group[each.key]["latest_version"]
  }

  scaling_config {
    desired_size = each.value["desired_size"]
    max_size     = each.value["max_size"]
    min_size     = each.value["min_size"]
  }

  lifecycle {
    ignore_changes = [scaling_config.0.desired_size]
  }

  tags = merge(local.common_tags, var.extra_tags)

  /* Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
     Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces */

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly
  ]
}

resource "aws_launch_template" "node_group" {
  for_each = {
    for node_group in var.node_groups : node_group["name"] => node_group
  }

  name     = "${var.name}-${each.value["name"]}-node-group"
  key_name = each.value["key_name"]

  network_interfaces {
    security_groups = compact(
      concat([aws_security_group.eks_worker_nodes_default_sg.id], each.value["additional_security_groups"])
    )
    associate_public_ip_address = each.value["associate_public_ip_address"]
  }

  monitoring {
    enabled = each.value["advanced_monitoring"]
  }

  tag_specifications {
    resource_type = "spot-instances-request"
    tags          = merge(local.common_tags, var.extra_tags)
  }

  tag_specifications {
    resource_type = "instance"
    tags          = merge(local.common_tags, var.extra_tags)
  }

  tags = merge(local.common_tags, var.extra_tags)

}

// Security Group:

resource "aws_security_group" "eks_worker_nodes_default_sg" {
  name        = "eks-worker-node-sg-${var.name}"
  description = "EKS worker node default SG"
  vpc_id      = var.vpc_id
  tags = merge(
    map("Name", "${var.name} EKS Worker Node"),
    local.common_tags,
    var.extra_tags
  )
}

resource "aws_security_group_rule" "worker_ingress_self" {
  security_group_id = aws_security_group.eks_worker_nodes_default_sg.id
  type              = "ingress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 65535
  self              = true
  description       = "Allow pods to communicate with each other"
}

resource "aws_security_group_rule" "worker_egress_self" {
  security_group_id = aws_security_group.eks_worker_nodes_default_sg.id
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 65535
  self              = true
  description       = "Allow pods to communicate with each other"
}

resource "aws_security_group_rule" "worker_ingress_control_plane" {
  security_group_id        = aws_security_group.eks_worker_nodes_default_sg.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 0 /* Don't limit it to just 10250, otherwise don't expect kubectl proxy to work properly */
  to_port                  = 65535
  source_security_group_id = aws_eks_cluster.cluster.vpc_config.0.cluster_security_group_id
  description              = "${var.name} EKS Control Plane"
}

resource "aws_security_group_rule" "worker_egress_cluster" {
  security_group_id        = aws_security_group.eks_worker_nodes_default_sg.id
  type                     = "egress"
  protocol                 = "tcp"
  from_port                = 443
  to_port                  = 443
  source_security_group_id = aws_eks_cluster.cluster.vpc_config.0.cluster_security_group_id
  description              = "${var.name} EKS Control Plane"
}

// IAM:

resource "aws_iam_role" "eks_worker_node" {
  for_each = {
    for node_group in var.node_groups : node_group["name"] => node_group
  }
  name               = "${join("", regexall("[[:alnum:]]", var.name))}${each.value["name"]}EKSWorkerNode"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy_for_eks_worker_nodes.json
  tags               = merge(local.common_tags, var.extra_tags)
}

data "aws_iam_policy_document" "assume_role_policy_for_eks_worker_nodes" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  for_each = {
    for node_group in var.node_groups : node_group["name"] => node_group
  }
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_worker_node[each.value["name"]]["name"]
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  for_each = {
    for node_group in var.node_groups : node_group["name"] => node_group
  }
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_worker_node[each.value["name"]]["name"]
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  for_each = {
    for node_group in var.node_groups : node_group["name"] => node_group
  }
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_worker_node[each.value["name"]]["name"]
}
