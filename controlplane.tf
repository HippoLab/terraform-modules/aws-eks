resource "aws_eks_cluster" "cluster" {
  name     = var.name
  role_arn = aws_iam_role.eks_control_plane.arn
  version  = var.kube_version

  vpc_config {
    endpoint_public_access  = var.endpoint_public_access
    endpoint_private_access = var.endpoint_private_access
    security_group_ids      = var.control_plane_additional_security_groups
    subnet_ids              = var.subnet_ids
    public_access_cidrs     = var.control_plane_public_access_cidrs
  }

  tags = merge(local.common_tags, var.extra_tags)

  /* Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
   Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces */

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.AmazonEKSServicePolicy,
    aws_iam_role_policy_attachment.ElasticLoadBalancingFullAccess
  ]
}

// Security Group:

resource "aws_security_group_rule" "eks_control_plane_worker_ingress" {
  security_group_id        = aws_eks_cluster.cluster.vpc_config.0.cluster_security_group_id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 443
  to_port                  = 443
  source_security_group_id = aws_security_group.eks_worker_nodes_default_sg.id
  description              = "${var.name} EKS worker nodes"
}

resource "aws_security_group_rule" "eks_control_plane_worker_egress" {
  security_group_id        = aws_eks_cluster.cluster.vpc_config.0.cluster_security_group_id
  type                     = "egress"
  protocol                 = "tcp"
  from_port                = 10250
  to_port                  = 10250
  source_security_group_id = aws_security_group.eks_worker_nodes_default_sg.id
  description              = "${var.name} EKS control plane"
}

// IAM:

resource "aws_iam_role" "eks_control_plane" {
  name               = "${join("", regexall("[[:alnum:]]", var.name))}EKSControlPlane"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy_for_eks_control_plane.json
  tags               = merge(local.common_tags, var.extra_tags)
}

data "aws_iam_policy_document" "assume_role_policy_for_eks_control_plane" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_control_plane.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks_control_plane.name
}

resource "aws_iam_role_policy_attachment" "ElasticLoadBalancingFullAccess" {
  policy_arn = "arn:aws:iam::aws:policy/ElasticLoadBalancingFullAccess"
  role       = aws_iam_role.eks_control_plane.name
}
