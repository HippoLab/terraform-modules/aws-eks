module "eks" {
  source     = "../."
  name       = "test-luster"
  subnet_ids = module.network.subnet_ids["eks"]
  vpc_id     = module.network.vpc_id
  extra_tags = local.common_tags
}

resource "aws_security_group_rule" "eks_worker_node_ec2_endpoint_egress" {
  security_group_id        = module.eks.eks_worker_default_sg_id
  type                     = "egress"
  protocol                 = "TCP"
  from_port                = 443
  to_port                  = 443
  source_security_group_id = aws_security_group.ec2_endpoint.id
}

resource "aws_security_group_rule" "eks_worker_node_ecr_api_endpoint_egress" {
  security_group_id        = module.eks.eks_worker_default_sg_id
  type                     = "egress"
  protocol                 = "TCP"
  from_port                = 443
  to_port                  = 443
  source_security_group_id = aws_security_group.ecr_api_endpoint.id
}

resource "aws_security_group_rule" "eks_worker_node_ecr_dkr_endpoint_egress" {
  security_group_id        = module.eks.eks_worker_default_sg_id
  type                     = "egress"
  protocol                 = "TCP"
  from_port                = 443
  to_port                  = 443
  source_security_group_id = aws_security_group.ecr_dkr_endpoint.id
}

resource "aws_security_group_rule" "eks_worker_node_s3_endpoint_egress" {
  security_group_id = module.eks.eks_worker_default_sg_id
  type              = "egress"
  protocol          = "TCP"
  from_port         = 443
  to_port           = 443
  prefix_list_ids   = [aws_vpc_endpoint.s3_vpc_endpoint.prefix_list_id]
}
