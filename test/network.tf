module "network" {
  source = "git::https://gitlab.com/HippoLab/terraform-modules/aws-network.git"
  name   = "test-network"
  subnets = [
    {
      name        = "public"
      ig_attached = true
      tags = map(
        "kubernetes.io/cluster/test-cluster", "shared",
        "kubernetes.io/role/elb", "1"
      )
    },
    {
      name        = "eks"
      ig_attached = false
      tags = map(
        "kubernetes.io/cluster/test-cluster", "shared",
        "kubernetes.io/role/internal-elb", "1"
      )
    }
  ]
  extra_tags = local.common_tags
}

// S3 VPC endpoint:

resource "aws_vpc_endpoint" "s3_vpc_endpoint" {
  service_name      = "com.amazonaws.${data.aws_region.current.name}.s3"
  vpc_endpoint_type = "Gateway"
  vpc_id            = module.network.vpc_id
  route_table_ids   = [module.network.route_table_ids["eks"]]
  tags = merge(
    map("Name", "Test S3 Endpoint"),
    local.common_tags,
  )
}

// ECR VPC endpoint:

resource "aws_vpc_endpoint" "ecr_dkr_endpoint" {
  service_name        = "com.amazonaws.${data.aws_region.current.name}.ecr.dkr"
  vpc_endpoint_type   = "Interface"
  vpc_id              = module.network.vpc_id
  subnet_ids          = module.network.subnet_ids["eks"]
  security_group_ids  = [aws_security_group.ecr_dkr_endpoint.id]
  private_dns_enabled = true
  tags = merge(
    map("Name", "Test ECR DKR Endpoint"),
    local.common_tags,
  )
}

resource "aws_security_group" "ecr_dkr_endpoint" {
  name        = "aws-vpc-ecr-dkr-endpoint"
  description = "ECR DKR VPC Endpoint"
  vpc_id      = module.network.vpc_id
  tags = merge(
    map("Name", "Test ECR DKR VPC endpoint"),
    local.common_tags,
  )
}

resource "aws_security_group_rule" "ecr_dkr_endpoint_ingress" {
  security_group_id = aws_security_group.ecr_dkr_endpoint.id
  type              = "ingress"
  protocol          = "TCP"
  from_port         = 443
  to_port           = 443
  cidr_blocks       = module.network.vpc_ipv4_cidr
}

resource "aws_vpc_endpoint" "ecr_api_endpoint" {
  service_name        = "com.amazonaws.${data.aws_region.current.name}.ecr.api"
  vpc_endpoint_type   = "Interface"
  vpc_id              = module.network.vpc_id
  subnet_ids          = module.network.subnet_ids["eks"]
  security_group_ids  = [aws_security_group.ecr_api_endpoint.id]
  private_dns_enabled = true
  tags = merge(
    map("Name", "Test ECR API Endpoint"),
    local.common_tags,
  )
}

resource "aws_security_group" "ecr_api_endpoint" {
  name        = "aws-vpc-ecr-api-endpoint"
  description = "ECR API VPC Endpoint"
  vpc_id      = module.network.vpc_id
  tags = merge(
    map("Name", "Test ECR API VPC endpoint"),
    local.common_tags,
  )
}

resource "aws_security_group_rule" "ecr_api_endpoint_ingress" {
  security_group_id = aws_security_group.ecr_api_endpoint.id
  type              = "ingress"
  protocol          = "TCP"
  from_port         = 443
  to_port           = 443
  cidr_blocks       = module.network.vpc_ipv4_cidr
}

// EC2 VPC endpoint:

resource "aws_vpc_endpoint" "ec2_endpoint" {
  service_name        = "com.amazonaws.${data.aws_region.current.name}.ec2"
  vpc_endpoint_type   = "Interface"
  vpc_id              = module.network.vpc_id
  subnet_ids          = module.network.subnet_ids["eks"]
  security_group_ids  = [aws_security_group.ec2_endpoint.id]
  private_dns_enabled = true
  tags = merge(
    map("Name", "Test EC2 Endpoint"),
    local.common_tags,
  )
}

resource "aws_security_group" "ec2_endpoint" {
  name        = "aws-vpc-ec2-endpoint"
  description = "EC2 VPC Endpoint"
  vpc_id      = module.network.vpc_id
  tags = merge(
    map("Name", "Test EC2 VPC endpoint"),
    local.common_tags,
  )
}

resource "aws_security_group_rule" "ec2_endpoint_ingress" {
  security_group_id = aws_security_group.ec2_endpoint.id
  type              = "ingress"
  protocol          = "TCP"
  from_port         = 443
  to_port           = 443
  cidr_blocks       = module.network.vpc_ipv4_cidr
}
