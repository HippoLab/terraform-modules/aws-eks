terraform {
  required_version = ">=0.13.0"
}

data "aws_region" "current" {}

locals {
  common_tags = {
    ManagedBy = "terraform"
  }
}
