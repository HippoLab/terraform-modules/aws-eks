AWS EKS terraform module
========================

Terraform module to create following AWS resources:
-
-
-
-

# Contents
- [Required Input Variables](#variables)
- [Usage](#usage)
- [Outputs](#outputs)
- [Licence](#licence)
- [Author Information](#author)

## <a name="variables"></a> Required Input Variables
At least following input variables must be provided. See [full list](variables.tf) of supported variables

| Name         | Description |
| ------------ | ----------- |

## <a name="usage"></a> Usage

## <a name="usage"></a> Usage
```hcl
module "eks" {
  source = "../modules/aws_eks"
  name   = "eks-cluster"
  vpc_id = module.network.vpc_id
  node_groups = [
    {
      name                        = "default"
      ami_type                    = "AL2_x86_64"
      instance_types              = [
        "t3a.xlarge",
        "t3.xlarge",
        "m5a.xlarge",
        "m5.xlarge"
      ]
      capacity_type               = "SPOT"
      subnet_ids                  = module.network.subnet_ids["public"]
      associate_public_ip_address = true
      additional_security_groups  = []
      advanced_monitoring         = false
      desired_size                = 3
      max_size                    = 3
      min_size                    = 3
    }
  ]
  subnet_ids = module.network.subnet_ids["eks"]
  extra_tags = local.common_tags
}

resource "aws_security_group_rule" "eks_worker_node_ipv4_egress" {
  security_group_id = module.eks.eks_worker_default_sg_id
  type              = "egress"
  protocol          = "all"
  from_port         = 0
  to_port           = 65535
  cidr_blocks       = [
    "0.0.0.0/0"
  ]
}

resource "aws_security_group_rule" "eks_worker_node_ipv6_egress" {
  security_group_id = module.eks.eks_worker_default_sg_id
  type              = "egress"
  protocol          = "all"
  from_port         = 0
  to_port           = 65535
  ipv6_cidr_blocks = [
    "::/0"
  ]
}
```

## <a name="outputs"></a> Outputs
Full list of module outputs and their descriptions can be found in [outputs.tf](outputs.tf)

## <a name="licence"></a> Licence
The module is being distributed under [MIT Licence](LICENCE.txt). Please make sure you have read, understood and agreed
to its terms and conditions

## <a name="author"></a> Author Information
Vladimir Tiukhtin <vladimir.tiukhtin@hippolab.ru><br/>London
