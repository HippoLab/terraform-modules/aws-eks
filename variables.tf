variable "name" {
  description = "Common cluster name"
  type        = string
}

variable "endpoint_public_access" {
  description = "Whether or not public access to EKS endpoint must be enabled"
  default     = true
}

variable "endpoint_private_access" {
  description = "Whether or not private access to EKS endpoint must be enabled"
  default     = true
}

variable "control_plane_public_access_cidrs" {
  description = "List of source IPs EKS api should accept requests from"
  type        = list(string)
  default     = null
}

variable "control_plane_additional_security_groups" {
  description = "Additional Security Group IDs to attach to an EKS Control Plane"
  type        = list(string)
  default     = []
}

variable "vpc_id" {
  type = string
}

variable "subnet_ids" {
  description = "Subnet IDs EKS endpoint and nodes must be started in"
  type        = list(string)
}

variable "node_groups" {
  description = ""
  type = list(object(
    {
      name                        = string
      ami_type                    = string
      instance_types              = list(string)
      subnet_ids                  = list(string)
      capacity_type               = string
      associate_public_ip_address = bool
      additional_security_groups  = list(string)
      advanced_monitoring         = bool
      key_name                    = optional(string)
      desired_size                = number
      max_size                    = number
      min_size                    = number
    }
  ))
  default = [
    {
      name     = "default"
      ami_type = "AL2_x86_64" // AL2_x86_64, AL2_x86_64_GPU or AL2_ARM_64
      instance_types = [      // t3.micro cannot handle more than 4 pods
        "t3a.small",
        "t3.small"
      ]
      capacity_type               = "SPOT" // SPOT or ON_DEMAND
      subnet_ids                  = null
      associate_public_ip_address = false
      additional_security_groups  = []
      advanced_monitoring         = false
      desired_size                = 3
      max_size                    = 6
      min_size                    = 3
    }
  ]
}

variable "kube_version" {
  description = "Kubernetes version to provision"
  type        = string
  default     = "1.18"
}

variable "extra_tags" {
  description = "Map of additional tags to add to module's resources"
  type        = map(string)
  default     = {}
}
