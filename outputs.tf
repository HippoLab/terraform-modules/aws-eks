output "endpoint" {
  value = aws_eks_cluster.cluster.endpoint
}
//
//output "ca_cert" {
//  value = base64decode(aws_eks_cluster.cluster.certificate_authority.0.data)
//}
//
//output "eks_cluster_role_name" {
//  value = aws_iam_role.eks_control_plane.name
//}
//
//output "eks_worker_role_name" {
//  value = aws_iam_role.eks_worker_node.name
//}
//
output "eks_worker_default_sg_id" {
  value = aws_security_group.eks_worker_nodes_default_sg.id
}
